%bcond_with test
Name:             perl-File-Copy-Recursive
Version:          0.45
Release:          2
Summary:          Perl extension for recursively copying files and directories
License:          GPL-1.0-or-later OR Artistic-1.0-Perl
URL:              https://metacpan.org/release/File-Copy-Recursive
Source0:          https://cpan.metacpan.org/modules/by-module/File/File-Copy-Recursive-%{version}.tar.gz
BuildArch:        noarch
Requires:         perl(File::Glob)
BuildRequires:    %{__perl} %{__make} perl-generators perl(Carp) perl(Cwd) perl(Exporter)
BuildRequires:    perl(ExtUtils::MakeMaker) perl(File::Copy) perl(File::Glob) perl(File::Spec)
BuildRequires:    perl(File::Temp) perl(Path::Tiny) perl(strict) perl(vars) perl(warnings)

%if %{with test}
BuildRequires:    perl(Test::Deep) perl(Test::Exception) perl(Test::Fatal) perl(Test::File)
BuildRequires:    perl(Test::More) perl(Test::Warnings)
%endif

%description
This module has 3 functions, one to copy files only, one to copy directories
only and one to do either depending on the argument's type.

%package help
Summary:          Help documentation for the %{name}

%description help
Help documentation for the %{name}.

%prep
%autosetup -n File-Copy-Recursive-%{version} -p1

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1
%make_build

%install
%{__make} pure_install DESTDIR=%{buildroot}
chmod -R u+w %{buildroot}/*

%check
%if %{with test}
%{__make} test
%endif

%files
%{perl_vendorlib}/*

%files help
%doc Changes README
%{_mandir}/man3/*

%changelog
* Sat Jan 18 2025 Funda Wang <fundawang@yeah.net> - 0.45-2
- drop useless perl(:MODULE_COMPAT) requirement

* Tue Jun 14 2022 SimpleUpdate Robot <tc@openeuler.org> - 0.45-1
- Upgrade to version 0.45

* Wed Jan 8 2020 likexin <likexin4@huawei.com> - 0.44-4
- package init
